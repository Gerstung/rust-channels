extern crate rand;

use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use std::time::{Instant};
use std::string::String;
use std::io;
use std::thread;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;

fn main() {

    let number_threads = input_number("Input number of threads");
    let number_strings = input_number("Input number of strings");
    let thread_iterations_each = number_strings / number_threads;

    let mut strings: Vec<String> = Vec::with_capacity(number_strings);
    let (tx, rx): (Sender<Vec<String>>, Receiver<Vec<String>>) = mpsc::channel();
    let mut children = Vec::new();

    // Start stopwatch
    let now = Instant::now();

    for thread in 0..number_threads {
        // The sender endpoint can be copied
        let thread_tx = tx.clone();

        // Each thread will send its id via the channel
        let child = thread::spawn(move || {
            // The thread takes ownership over `thread_tx`
           
            let mut s = Vec::with_capacity(thread_iterations_each);
            for _ in 0..thread_iterations_each {
                s.push(generate_string(8));
            }
           
            // Each thread queues a message in the channel
            thread_tx.send(s).unwrap();

            // Sending is a non-blocking operation, the thread will continue
            // immediately after sending its message
            println!("thread {} finished", thread);
        });

        children.push(child);
    }


    // Here, all the messages are collected
    for _ in 0..number_threads {
        // The `recv` method picks a message from the channel
        // `recv` will block the current thread if there are no messages available
        strings.append(&mut rx.recv().unwrap());
    }
    
    // Wait for the threads to complete any remaining work
    for child in children {
        child.join().expect("oops! the child thread panicked");
    }

    // Show the order in which the messages were sent
    let elapsed = now.elapsed().as_millis();
    println!("We have {} strings generated in {} ms", strings.len(), elapsed);

    let _ = input_number("Press any key to continue");

}

fn generate_string(size: usize) -> String {
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(size)
        .collect();

    rand_string
}

fn input_number(s: &str) -> usize {
    println!("{}", s);
    let mut in_nr = String::new();
    io::stdin().read_line(&mut in_nr)
        .expect("Failed to read line");

    let in_nr: usize = match in_nr.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("This was not a valid usize number!"),
    };

    in_nr
}